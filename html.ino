/*
 * Functions to handle web page and web socket data
 * 
 * 
 */


//Get Slider Values  
//only sliderValue1 is used, for pump power
String getSliderValues(){
  sliderValues["sliderValue1"] = String(sliderValue1);
  sliderValues["sliderValue2"] = String(sliderValue2);
  String jsonString = JSON.stringify(sliderValues);
  return jsonString;
}


void notifyClients(String sliderValues) {
  ws.textAll(sliderValues);
}



//********************  Function to process data from web page.  Either slider value or button push

void handleWebSocketMessage(void *arg, uint8_t *data, size_t len) {
  AwsFrameInfo *info = (AwsFrameInfo*)arg;
  if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT) {
    data[len] = 0;
    message = (char*)data;
    if (message.indexOf("1s") >= 0) {    //if this is 1s, then its power slider data  
      sliderValue1 = message.substring(2);
      dutyCycle1 = map(sliderValue1.toInt(), 0, 100, 0, 255);
      dacWrite(DAC1, dutyCycle1);
 //     Serial.println(dutyCycle1);
 //     Serial.print(getSliderValues());
 //     notifyClients(getSliderValues());
    }
    if (message.indexOf("2s") >= 0) {   //if this is 2s, then its other slider data  
      sliderValue2 = message.substring(2);
   //   dutyCycle2 = map(sliderValue2.toInt(), 0, 100, 0, 255);
   //   Serial.println(dutyCycle2);
  //      Serial.println(getSliderValues());
  //    notifyClients(getSliderValues());
    }   
    if (message.indexOf("3s") >= 0) {    //if this is 3s, then its top button
 //       Serial.println("toggle button");
        StartTime = millis();
    }  

    if (message.indexOf("4s") >= 0) {    //if this is 4s, then its push button to enter OTA mode  
        Serial.println("button");
        OTAWebPage();
 //       GetFileName(); 
    }  
    if (strcmp((char*)data, "getValues") == 0) {
 //     notifyClients(getSliderValues());
    }
  }
}

//**********************************************************************************************
void onEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len) {
  switch (type) {
    case WS_EVT_CONNECT:
      Serial.printf("WebSocket client #%u connected from %s\n", client->id(), client->remoteIP().toString().c_str());
      break;
    case WS_EVT_DISCONNECT:
      Serial.printf("WebSocket client #%u disconnected\n", client->id());
      break;
    case WS_EVT_DATA:
      handleWebSocketMessage(arg, data, len);
      break;
    case WS_EVT_PONG:
    case WS_EVT_ERROR:
      break;
  }
}

void initWebSocket() {
  ws.onEvent(onEvent);
  server.addHandler(&ws);
}
