# ESP32 Espresso Webserver (ESP32WebSocketSlider.ino)

ESP32 program for my HX espresso machine.  This is one of two ESP32's used for this project.

[Sites link for main project](https://sites.google.com/view/coffee4randy/projects/hx-dumb-to-smart)

[Sites link for this program](https://sites.google.com/view/coffee4randy/projects/hx-dumb-to-smart/webserver-esp32)

This ESP32 performs the following functions:

- Generate a web page with data from espresso machine, temperature, pressure, flow rate, timers, etc.  HTML, script and style files for web page are stored on sd card.  Uses asynchronous web server and web sockets.  Updates data on page once a sec.  Go to http://192.168.xxx.xxx to call up the page, where xxx.xxx is the end of the ip address.  I setup it as a static address with my router so the address won't change.
- Recieve slider data from  web page slider to set vibe pump power.  Default pump power at start is full power.
- Control vibe pump power by setting DAC output voltage on GPIO25.  This voltage is read by an arduino nano that is controlling the AC pump voltage.
- Receive data from other ESP32 (ESP32Flow) via ESP now protocol.  
- Create and save a data file of shot data to SD card, after shot is finished.  Shot data is saved once per second from beginning of water flow until 10 secs after flow ends. There is logic to decide if water was flowing to pull a shot, versus flowing to flush the HX or clean the screens.  Only saves files after a shot, deletes the file if it wasn't a shot.
- Send email with shot data file after shot is finished.
- Data file name is created using date/time information from NTP
- Support OTA program mode for wifi program updates.  There is a button on the bottom of webpage to enable OTA, then go to http://192.168.xxx.xxx/update to call up OTA page.


**Program notes:  **
Had problems with NTP.  When I tried to call NTP functions during the program, they would cause the rest of the routine to not work.  So I set date at start of program, and remember the time at start.  The date file name uses the date from program start, and time is calcuated by adding run time to the remembered start time. More complicated than it should be, but it works. 

ESP Now only worked for ESP32 to ESP32.  I could not get a ESP8266 to talk to a ESP32.  So I used two ESP32's for this project.


**Files required for build.**
- ota.ino contains routine for starting ota mode
- html.ino contains routines for webpage and websockets.
- credentials.h contains data that I don't want to share, such as wifi log in info.  So this is a sanitized version.

Also need to setup the webpage data, see the Filesystem OTA section here for how to do that [Sites link for this program](https://sites.google.com/view/coffee4randy/projects/hx-dumb-to-smart/webserver-esp32)
Files for webpage are in the data subdirectory

**SD Card setup**
Format card as fat32


NOTES:  
