/* 
 *  Version info:  added weight data and formatted file to include
 *  
 *  ESP32Web Server program by Randy T
 *  Program features
 *    Analog output from DAC to the arduino nano to set pump power
 *    Recieves hx data over esp now from sensor esp32 including temp, pressure, flow ...
 *    Recieves weight data over esp now from bluetooth scale esp32
 *    Create webpage with slider to input pump power command, displays hx data    
 *    Creates data file with shot info
 *    sends data file via email  
 *    supports OTA programming.  Push button on bottom of webpage to enable
 *    OTA programming can also program SPIFFS memory where webpage design files are stored
 *    Stores shot data files to uSD card
 *  
 *  
  Based on:
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp32-web-server-websocket-sliders/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*/


/*
 * ***************Section for defining libraries, constants and global variables
 */
 
#include <Arduino.h>
#include <esp_now.h>                 //for esp now protocol
#include <WiFi.h>                    //for the esp32 wifi interface
#include <AsyncTCP.h>                //for the async web server
#include <ESPAsyncWebServer.h>       //for the async web server
#include "ESP32_MailClient.h"        //for sending emails

//

//these are for file system on SD and SPIFFS
#include "FS.h"
#include "SD.h"
#include "SPIFFS.h"

#include "SPI.h"  //for spi interface to talk to micro sd interface adpater

#include <Arduino_JSON.h>            //to create json formatted variables

//lib for ota
#include <AsyncElegantOTA.h>

//lib for getting time from NTP
#include <NTPClient.h>
#include <WiFiUdp.h>

#include <TelnetStream.h>  //for telenet for wireless debugging 


/* WiFi Credentials */
#include "Credentials.h" //wifi info 

//**************pinout definition
#define DAC1 25      //gpio pin to output analog value to set pump power   

//pins for spi interface to connect to SD card:
//sd card cs   GPIO 5
//sd card mosi GPIO 23
//sd card clk  GPIO 18
//sd card miso GPIO 19

// for ota
const char* host = "esp32 expobar";

//Structure used to receive ESP Now data
//Must match the transmitter structure
typedef struct struct_message {
    int Sid;
    double StempGroup;
    double StempBoiler;
    double Srate;       //ml per sec from flowmeter
    double StotalML;    //total ml from flowmeter for this shot
    double Spressure;
    double Sweight;    //weight from bluetooth scale
    int StimerPump;    //not used, used timer from start of flow instead
    int StimerFlow;
    int SreadingId;
} struct_message;

struct_message incomingReadings;  //variable to store the incoming data

//variables that store the ESP now data, updated with latest data
    int Vid;
    double TempGroup = 1;
    double TempBoiler = 2;
    double Rate = 0;       //ml/s from flowmeter
    double totalML= 0;
    double Pressure = 0;
    double Weight = 0;
    int VarTimerPump = 0;
    int VarTimerFlow = 0;
    int VarTimerStBut = 0;  //timer from pushing start button from webpage
    int VreadingId;


//Structure used for circular buffer for history data
//saves the last 10 seconds of data in a circular buffer 
typedef struct history_struc {
    int Sid;
    double StempGroup;
    double StempBoiler;
    double Srate;       //ml/s from flowmeter
    double StotalML;    //total ml from flowmeter for this shot
    double Spressure;
    double Sweight;    //weight from scale
    int StimerButton;    //not used, used timer from start of flow instead
    int StimerFlow;
    String SsliderValue1;
    int SreadingId;
} ;

#define history_size    20   //size of history array

history_struc history_array[history_size];  //create the actual buffer

int history_ptr=0;   //input pointer for the buffer
int out_ptr;         //output pointer used to send history data to data file

struct tm timeinfo;  //used to store time from NTP

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

// Create a WebSocket object
AsyncWebSocket ws("/ws");

String message = "";
String sliderValue1 = "70";   //set pump to full power at power on
String sliderValue2 = "100";

//Json Variable to Hold Slider Values
JSONVar sliderValues;

//Json Variable to Hold Values to display on webserver page
JSONVar displayValues;

//Json Variable to store incoming data from esp now interface
JSONVar inValues;

long currentMillis = 0;
long previousMillis = 0;
int interval = 1000;
double StartTime = 0;
double StartFlowTime = 0;
int dutyCycle1 = 0;
long lastflowMillis = 0;
long start_flow = 0;
boolean flowon_flag = false;
boolean shotStarted_flag = false;
boolean saveShot_flag = false;

//for NTP server support to get time and date
const long  gmtOffset_sec = -28800;  //-8 hours offset for PST, calc for your timezone
const int   daylightOffset_sec = 3600;  //one hour offset for daylight savings
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);
String formattedDate;       //save current date
String dayStamp;
String timeStartUp;  //time at startup
long SecStartUp;  //Secs at startup
long MinStartUp;  //mins at startup
long HourStartUp;  //hours at startup
long TotalSecStartUp;  //convert hours and mins to secs, total it all

//strings for saving data to data file
 String dataString = "";  //string to create one line of data, for writing to data file
 String file_name = "";    //sting to save data file name, uses date to make the file name
  
File myFile;    //used for creating the data file

 //for sending emails
#define smtpServer            "smtp.gmail.com"
#define smtpServerPort        465
#define emailSubject          "ESP32 espresso data" 

// Email Sending data object contains config and data to send
SMTPData smtpData;

/*
 * ************End of defining libraries, constants and global variables
 */


/**********************************************************************************************
 * ********************************** . Section of functions . ********************************
 **********************************************************************************************/
 
//********************* OnDataRecv Function to receive esp now data ******************************************
// callback function that will be executed when data is received
void OnDataRecv(const uint8_t * mac_addr, const uint8_t *incomingData, int len) { 
  // Copies the sender mac address to a string
  char macStr[18];

  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);

//copy the incoming data to a temporary structure (incomingReadings)
  memcpy(&incomingReadings, incomingData, sizeof(incomingReadings));

  if (incomingReadings.Sid == 1) {   //id is 1 for bluetooth scale esp32
    Weight = incomingReadings.Sweight;
  }
  
  if (incomingReadings.Sid == 2) {   //id is 2 for esp32 that collects data
    Vid = incomingReadings.Sid;
    TempGroup = incomingReadings.StempGroup;
    TempBoiler = incomingReadings.StempBoiler;
    Rate = incomingReadings.Srate;       //ml/s from flowmeter
    totalML = incomingReadings.StotalML;    //total ml from flowmeter for this shot
    Pressure = incomingReadings.Spressure;
    VarTimerPump = incomingReadings.StimerPump;    //
    VarTimerFlow = incomingReadings.StimerFlow;
    VreadingId = incomingReadings.SreadingId;

//save all data in circular history buffer
    history_array[history_ptr].Sid = Vid;
    history_array[history_ptr].StempGroup = TempGroup;
    history_array[history_ptr].StempBoiler = TempBoiler;
    history_array[history_ptr].Srate = Rate;
    history_array[history_ptr].StotalML = totalML;
    history_array[history_ptr].Spressure = Pressure;
    history_array[history_ptr].Sweight = Weight;
    history_array[history_ptr].StimerButton = VarTimerStBut;
    history_array[history_ptr].StimerFlow = VarTimerFlow;
    history_array[history_ptr].SsliderValue1 = sliderValue1;

    history_ptr++;
    
    //history_array is circular buffer, set pointer to beginning if at max
    if (history_ptr >= (history_size) ) {
      history_ptr = 0;
    }


// Code to see if shot is starting, and initialize some stuff
    if (Rate > 0) {   //check if any water is flowing
      lastflowMillis = millis();
      if (flowon_flag == false) { //if this is false, flow/shot is starting now
        start_flow = lastflowMillis;  //reset flow timer
        flowon_flag = true;
        shotStarted_flag = true;
        GetNameFile();  //open data file to save shot data
        }
      //the logic below is to decide if it really is a shot, versus turning on to flush or clean or something  
      if ((Pressure > 4) || ((Pressure > 1.5) && ((sliderValue1.toInt()) < 31) ) ) {   //set flag to save shot data if rate is positive  and high pressure
        //note that if pressure is low when flow is on for entire time, not pulling shot
        saveShot_flag = true;
        }
   //   LastRate = Rate;
      }
    else {  //no water flowing, see if in prefusion
      if ( (shotStarted_flag == true) && (sliderValue1.toInt() < 25) ) {  //sliderValue1 is pump power, if power is low, flow may stop  
        lastflowMillis = millis();    //if in prefusion, reset time for last flow.
      }  
    }

//this code is to decide if the shot is finished.  Was just based on time since flow, 
//but added check for sliderValue1 to make sure not in prefusion mode because sometimes no flow in prefusion

    //if no flow for 10 secs, then shot is over.
    if ( ( (millis() - lastflowMillis) > 10000) && (sliderValue1.toInt() > 30) )  {   
      flowon_flag = false;
    }
  }

  //timeout of 100 secs, in case there is a problem, to stop the shot logic
 // if ( (millis() - start_flow) > 100000) {
 //   flowon_flag = false; 
 // }
}


//******************Function to create json variable to send to webpage **************************************************
// converts data to strings, then 
String getDisplayValues(){
  VarTimerStBut = (millis()-StartTime)/1000;   //calculate Timer Start Button time

  //convert data to strings, and store in json variable
  displayValues["totalML"] = String(totalML); 
  displayValues["pres"] = String(Pressure);
  displayValues["rate"] = String(Rate);
  displayValues["tempBoiler"] = String(TempBoiler);
  displayValues["tempGroup"] = String(TempGroup);
  displayValues["timerPump"] = String(VarTimerFlow);  //no timer pump, use timer flow
  displayValues["timerBut"] = String(VarTimerStBut);  
  displayValues["weight"] = String(Weight);  // Weight 
  displayValues["timerFlow"] = String(VarTimerFlow);   

//convert to json, and return it
  String displayString = JSON.stringify(displayValues);
  return displayString;
}

// ********************Function to Initialize SD ***************************************
void initSDCard(){
  
  if(!SD.begin()){
    Serial.println("Card Mount Failed");
    TelnetStream.print("Card Mount Failed");
    return;
  }
  uint8_t cardType = SD.cardType();

  if(cardType == CARD_NONE){
    Serial.println("No SD card attached");
    TelnetStream.print("No SD card attached");
    return;
  }

 // Serial.print("SD Card Type: ");
  if(cardType == CARD_MMC){
 //   Serial.println("MMC");
  } else if(cardType == CARD_SD){
 //   Serial.println("SDSC");
  } else if(cardType == CARD_SDHC){
 //   Serial.println("SDHC");
  } else {
 //   Serial.println("UNKNOWN");
  }
  uint64_t cardSize = SD.cardSize() / (1024 * 1024);
  Serial.printf("SD Card Size: %lluMB\n", cardSize);
    
}

//*********************** Function to Initialize WiFi  ********************************************
void initWiFi() {
    // Set the device as a wifi Station and Soft Access Point simultaneously
    //need these wifi modes for webserver and to receive wifi now data
  WiFi.mode(WIFI_AP_STA);
  
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Setting as a Wi-Fi Station..");
  }
  Serial.print("Station IP Address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Wi-Fi Channel: ");
  Serial.println(WiFi.channel());  
}

//***************** Writes one line of data to the data file ************************************
// uses data from circular history buffer.

void WriteDataLine (int data_ptr){   //pass data_ptr to define which element from history buffer to write  Normally last one

  char tempStr[5];  
  
  //need to convert all data to string data, then append to dataSting
  //add spaces to format for viewing email in google gmail
  
    //write timerFlow
    dataString = "  ";                        //add spaces to format for viewing
    sprintf( tempStr, "%3d", history_array[data_ptr].StimerFlow );   //format int to 3 char string
    dataString += String(tempStr);                                  // add data to string
    dataString += ",    ";                                          //add , and spaces to format for viewing
    
    //write totalML (volume)
    dtostrf(history_array[data_ptr].StotalML, 4, 1, tempStr);      //format double float to xx.x char string 
    dataString += String(tempStr); 
    dataString += ", ";                                           //add , and spaces to format for viewing
    
    //write pressure
    dtostrf(history_array[data_ptr].Spressure, 4, 1,  tempStr);    //format double float to xx.x char string 
    dataString += String(tempStr);
    dataString += ", ";                                            //add , and spaces to format for viewing
    
    //write rate
    dtostrf(history_array[data_ptr].Srate, 4, 1, tempStr);         //format double float to xx.x char string 
    dataString += String(tempStr);
    dataString += ",    ";
    
    //write weight
    dtostrf(history_array[data_ptr].Sweight, 4, 1, tempStr);         //format double float to xx.x char string 
    dataString += String(tempStr);
    dataString += ",  ";

    //write TempGroup
    dtostrf(history_array[data_ptr].StempGroup, 5, 1,  tempStr);    //format double float to xxx.x char string 
    dataString += String(tempStr);
    dataString += ",   ";

    //write TempBoiler
    dtostrf(history_array[data_ptr].StempBoiler, 5, 1,  tempStr);   //format double float to xxx.x char string 
    dataString += String(tempStr);
    dataString += ",     ";
    
    //write TimerStBut
    sprintf( tempStr, "%5d", history_array[data_ptr].StimerButton );
    dataString += String(tempStr);   
    dataString += ",      ";
    
    //write sliderValue1
    dataString += String(history_array[data_ptr].SsliderValue1);  

         
//history_array[data_ptr].Sweight

 // if the file is available, write the line to it:
    if (myFile) {
      myFile.println(dataString);
      } 
  // if the file isn't open, pop up an error:
    else {
      TelnetStream.print("error writing datalog.txt");
      //Serial.println("error writing datalog.txt");
  }    
}


//************************** Function to get date/time to use for data file name  ******************
//***************************open data file and create header row at top *******************************
void GetNameFile(){

  long tempTime = (millis()/1000) + TotalSecStartUp;  //add current secs to startup time for current time

  //convert time in total secs to time in secs, mins, hours
  long tempSec = (tempTime % 60) ;
  tempTime = (tempTime - tempSec)/60;
  long tempMin = tempTime % 60;
  tempTime = (tempTime - tempMin)/60;
  long tempHour = tempTime;

//  file name format is "/yyyy-mm-dd_hh_mm.txt"
  file_name = "/";
  file_name += dayStamp;
  file_name += "_";
  file_name += String(tempHour);
  file_name += "_";
  file_name += String(tempMin);
  file_name += ".txt";

    // Open the data file on the SD card so ready to write
  myFile = SD.open(file_name, FILE_WRITE);

  //Create and write header row as first line of the data file.  
  dataString = "TimerFlow";
  dataString += ", ";
  dataString += "Vol"; 
  dataString += ", ";
  dataString += "Pres";
  dataString += ", ";
  dataString += "Rate";
  dataString += ",  ";
  dataString += "Weight";
  dataString += ",  ";
  dataString += "Group";
  dataString += ",  ";
  dataString += "Boiler";
  dataString += ", ";
  dataString += "Timer Button";  
  dataString += ", ";
  dataString += "Power";
  
  myFile.println(dataString);

  /*This code writes data from 2 seconds before flow starts, in this order:
   * 2 secs old
   * 1 sec old
   * 
   * It has has to account for a circular buffer, so when history_ptr is close to 0 and buffer size
   * logic had to adjust to wraparound too
   */
 
    if (history_ptr == 2) {
      out_ptr = (history_size-1) ;
    }  
    else if (history_ptr == 1) {
      out_ptr = (history_size-2) ;
    } 
    else if (history_ptr == 0) {
      out_ptr = (history_size-3) ;
    } 
    else {
      out_ptr = history_ptr-3;
    }
    
    for (int i = 1; i < 3; i++) {   //write 2 lines of data, last 2 secs of history
      WriteDataLine (out_ptr);
      out_ptr++;
      if (out_ptr == history_size) {
        out_ptr = 0;
      }
    }

  // if the file had error
  if (!myFile) {
    // if the file didn't open, print an error:
    Serial.println("error opening data file");
  }        
}



//********************* Write data to file  ************************************************************
//                called once per sec to get one line each sec
void WriteDataFile(){

  if (flowon_flag == true) { //if flowon flag set, write next line of data to file

    //set temp pointer to point to last data sample.  Logic to account for wrap around of data pointer, and history_ptr incremented before this 
    int temp_ptr;
    if (history_ptr == 0) {
      temp_ptr = history_size-1;
    }
    else {
      temp_ptr = history_ptr-1;
    }
    WriteDataLine (temp_ptr);
  }
  
  //
  //logic here for end of shot and saving data file to email
  
  else {    //else there is no water flowing for XX secs, shot is over  
    if (shotStarted_flag == true) {
      shotStarted_flag = false;
      myFile.close();       //close the data file
      if (saveShot_flag == false) {
        delay(500);
        //delete file, don't care since not a shot
        SD.remove(file_name);
      }
      else {
        saveShot_flag = false;
        sendEmail();    //send email with data file attached
      }
    }
  }
}
 

/***************** Function to send email with data file attached */

void sendEmail(){
  // Set the SMTP Server Email host, port, account and password
  // account and password in credentials.h
  smtpData.setLogin(smtpServer, smtpServerPort, emailSenderAccount, emailSenderPassword);
  
  // Set the sender name and Email
  smtpData.setSender("ESP32-CAM", emailSenderAccount);
  
  // Set Email priority or importance High, Normal, Low or 1 to 5 (1 is highest)
  smtpData.setPriority("High");

  // Set the subject
  smtpData.setSubject(emailSubject);
    
  // Set the email message in text format
  smtpData.setMessage("Espresso data file attached.", false);

  // Add recipients, can add more than one recipient
  smtpData.addRecipient(emailRecipient);           //in credentials.h
  //smtpData.addRecipient(emailRecipient2);

  // Add attach files from SPIFFS
  smtpData.addAttachFile(file_name, "text/plain");
  // Set the storage type to attach files in your email (SPIFFS)
  smtpData.setFileStorageType(MailClientStorageType::SD);

  smtpData.setSendCallback(sendCallback);
  
  // Start sending Email, can be set callback function to track the status
  if (!MailClient.sendMail(smtpData))
    Serial.println("Error sending Email, " + MailClient.smtpErrorReason()); 
}

// Callback function to get the Email sending status
void sendCallback(SendStatus msg) {
  //Print the current status
 // Serial.println(msg.info());
}


/**********************************************************************************************
 * ********************************** . Setup and Main loop . *********************************
 **********************************************************************************************/
 
//*********************************** SETUP ****************************************************
void setup() {
  Serial.begin(115200);

 // Serial2.begin(19200);  //tried serial 2 port to get weight data from bt scale ESP32, didn't work


  initSDCard();   //call function to init sd card
  
  initWiFi();    //call function to init wifi

//init spiffs
    if(!SPIFFS.begin(true)){
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }
  
  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
 //   Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_register_recv_cb(OnDataRecv);
  
  initWebSocket();

//init analog output for pump control
  dutyCycle1 = map(sliderValue1.toInt(), 0, 100, 0, 255);
  dacWrite(DAC1, dutyCycle1);

 // Init routines to get time from NTP.  Used for data file names.
  timeClient.begin();
  int timeOffset = gmtOffset_sec + daylightOffset_sec;  //calc offset from GMT
  timeClient.setTimeOffset(timeOffset);
  
  while(!timeClient.update()) {
    timeClient.forceUpdate();
    }
  
  formattedDate = timeClient.getFormattedDate();  //sets to string of current date
  int splitT = formattedDate.indexOf("T");
  dayStamp = formattedDate.substring(0, splitT);

  SecStartUp = timeClient.getSeconds();  //get secs at startup
  MinStartUp= timeClient.getMinutes();  //get mins at startup
  HourStartUp = timeClient.getHours();  //get hours at startup
  TotalSecStartUp = SecStartUp + (MinStartUp * 60) + (HourStartUp * 60 * 60);  //convert startup time to total secs
  
  // Web Server Root URL
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/index.html", "text/html");
  });
  
  server.serveStatic("/", SPIFFS, "/");

  // Start server
  server.begin();

// intialize slider values
  notifyClients(getSliderValues());

//telnet used for debugging over wifi
  TelnetStream.begin();

}


//**********************main loop ******************************************************
void loop() {
  currentMillis = millis();
  if (currentMillis - previousMillis > interval) {
    previousMillis = millis();
    notifyClients(getDisplayValues());       //updates the webpage data
    WriteDataFile();                         //if pulling shot, writes line of data to file
    ws.cleanupClients();                     //websocket cleanup is recommended to do

    //this code resets the esp32 every day at 1am
    long TimeSecs = (millis()/1000) + TotalSecStartUp;
    if (TimeSecs > 90000) {   //if its after 1am, reset
      ESP.restart();
    }
  }

  //telenet code for debugging
    switch (TelnetStream.read()) {
    case 't':
      TelnetStream.println("telnet on");
      //TelnetStream.stop();
      break;
    case 's':
      TelnetStream.println("save shot");
      saveShot_flag = true;
      //TelnetStream.stop();
      break;

 
  }

}
