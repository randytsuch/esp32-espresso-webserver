// Complete project details: https://randomnerdtutorials.com/esp32-web-server-websocket-sliders/

var gateway = `ws://${window.location.hostname}/ws`;
var websocket;
window.addEventListener('load', onload);

function onload(event) {
    initWebSocket();
}

function getValues(){
    websocket.send("getValues");
}

function initWebSocket() {
    console.log('Trying to open a WebSocket connection…');
    websocket = new WebSocket(gateway);
    websocket.onopen = onOpen;
    websocket.onclose = onClose;
    websocket.onmessage = onMessage;
}

function onOpen(event) {
    console.log('Connection opened');
    getValues();
}

function onClose(event) {
    console.log('Connection closed');
    setTimeout(initWebSocket, 2000);
}

//function updateSliderPWM(element) {
function sendUIdata(element) {
    var msgNumber = element.id.charAt(element.id.length-1);
    var msgValue = document.getElementById(element.id).value;
    console.log("msgValue: ");
    console.log(msgValue);
    console.log("msgNumber: ");
    console.log(msgNumber);
    if (msgNumber == '1' || msgNumber == '2') {
        console.log("value");
        document.getElementById("sliderValue"+msgNumber).innerHTML = msgValue;
        websocket.send(msgNumber+"s"+msgValue.toString());
        }
    else {
        websocket.send(msgNumber+"s"+msgValue.toString());
    }
 //   document.getElementById("sliderValue"+msgNumber).innerHTML = msgValue;
//    websocket.send(msgNumber+"s"+msgValue.toString());

}

function onMessage(event) {
    console.log(event.data);
    var myObj = JSON.parse(event.data);
    var keys = Object.keys(myObj);
    console.log ("keys length: ");
    console.log(keys.length);
    for (var i = 0; i < keys.length; i++){
        var key = keys[i];
        var element = document.getElementById('sliderValue1').value;
        console.log ("element is ");
        console.log (element);
        if (element != null) {
           console.log("element");
           document.getElementById(key).innerHTML = myObj[key];
           document.getElementById("slider"+ (i+1).toString()).value = myObj[key];
           }
        else {
           document.getElementById(key).innerHTML = myObj[key];
           document.getElementById("tempGroup").value = myObj[key];
           }
    }
}


function onButtonToggle() {
    alert('Hello World!');
   //  document.getElementById("demo").innerHTML = "Hello World";
}
